-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 18, 2020 at 05:48 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `belajar_blog`
--

-- --------------------------------------------------------

--
-- Table structure for table `blog`
--

CREATE TABLE `blog` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `category` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blog`
--

INSERT INTO `blog` (`id`, `title`, `content`, `category`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 'cara memasak air', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempore sequi explicabo laborum nulla earum perferendis natus eveniet. Vitae, cumque maxime repudiandae veritatis, dolorum odit adipisci aspernatur, officia non iure ullam!.', 'Tutorial', NULL, NULL, NULL),
(2, 'cara memasak air', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempore sequi explicabo laborum nulla earum perferendis natus eveniet. Vitae, cumque maxime repudiandae veritatis, dolorum odit adipisci aspernatur, officia non iure ullam!.', 'Tutorial', NULL, NULL, NULL),
(3, 'cara memasak air', 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempore sequi explicabo laborum nulla earum perferendis natus eveniet. Vitae, cumque maxime repudiandae veritatis, dolorum odit adipisci aspernatur, officia non iure ullam!.', 'Tutorial', NULL, NULL, NULL),
(4, 'Dr.', 'Et error minus voluptatibus illum excepturi nesciunt ad. Totam et id corrupti voluptas. Id et animi sit molestiae.', 'Physician Assistant', '2020-09-16 07:13:14', '2020-09-16 07:13:14', NULL),
(5, 'Mr.', 'Sed ab dolores accusantium qui. Ut ut ad voluptatem natus consequatur voluptatem. Laudantium sit possimus alias aut aut dicta.', 'Automotive Specialty Technician', '2020-09-16 07:13:14', '2020-09-16 07:13:14', NULL),
(6, 'Mr.', 'Rerum consequatur et quos. Consequatur delectus maiores sint. Fuga assumenda ut ut voluptatum quia. Dolorem odit rerum harum.', 'Rolling Machine Setter', '2020-09-16 07:13:14', '2020-09-16 07:13:14', NULL),
(7, 'Prof.', 'Sapiente itaque perferendis ea non quia tempore quas. Consequatur sint magni recusandae nulla est. Perferendis ab ullam et qui eum.', 'Financial Analyst', '2020-09-16 07:13:15', '2020-09-16 07:13:15', NULL),
(8, 'Mr.', 'Temporibus vitae accusantium illum nobis dicta amet quia. Rerum ullam quo qui vel et rerum beatae. Amet perspiciatis molestiae et. Inventore quam minus sint est odit et hic. Et voluptate qui voluptates asperiores facilis illum dolores.', 'Plasterer OR Stucco Mason', '2020-09-16 07:13:15', '2020-09-16 07:13:15', NULL),
(9, 'Mr.', 'Consectetur ut error accusamus nostrum quos. Aut est libero vel beatae repellat id aspernatur. Vel excepturi expedita incidunt illo.', 'Metal Pourer and Caster', '2020-09-16 07:13:15', '2020-09-16 07:13:15', NULL),
(10, 'Dr.', 'Ratione minus dolor ipsam id natus velit voluptatibus rerum. Iure rerum autem tenetur et repellat. Sit vel deleniti sed facere. Doloremque odio repudiandae alias recusandae aperiam nisi quia.', 'Lodging Manager', '2020-09-16 07:13:15', '2020-09-16 07:13:15', NULL),
(11, 'Mrs.', 'Qui et possimus quia non laudantium minus debitis. Nesciunt quia enim voluptatem ut. Est et aliquam modi pariatur aut sit. Consequatur aut ab suscipit aspernatur.', 'Production Planning', '2020-09-16 07:13:15', '2020-09-16 07:13:15', NULL),
(12, 'Miss', 'Minima consequatur occaecati tempore sint fugiat voluptatem animi. Rerum explicabo et ratione odio totam eius. Distinctio molestiae nisi autem odio. Quibusdam eum impedit non atque.', 'Radiologic Technician', '2020-09-16 07:13:15', '2020-09-16 07:13:15', NULL),
(13, 'Dr.', 'Sit error sequi et facilis. Velit sed ipsam aperiam aut dolores et. Excepturi ad sed et hic eos blanditiis. Provident eaque dicta est enim.', 'Human Resources Manager', '2020-09-16 07:13:15', '2020-09-16 07:13:15', NULL),
(14, 'Mrs.', 'Dolorem optio in quo iure modi. Impedit recusandae eos at inventore tempore. Veniam eaque perferendis qui voluptate repellendus voluptatibus autem labore. Numquam blanditiis est doloribus qui. Earum quo consequuntur error deleniti tempore laboriosam.', 'Telecommunications Line Installer', '2020-09-16 07:13:15', '2020-09-16 07:13:15', NULL),
(15, 'Dr.', 'Recusandae dolore ex enim velit blanditiis. Perferendis quis a aut explicabo deleniti. Officia quasi eos dicta sint at dolor odio aperiam. Nostrum sit voluptatem dolores odit inventore quia.', 'Order Filler OR Stock Clerk', '2020-09-16 07:13:15', '2020-09-16 07:13:15', NULL),
(16, 'Prof.', 'Dolores cumque ratione est aspernatur et. Et optio praesentium quibusdam aut. Reiciendis saepe optio deleniti corporis et dolore atque numquam.', 'Dental Hygienist', '2020-09-16 07:13:15', '2020-09-16 07:13:15', NULL),
(17, 'Ms.', 'Vero suscipit officia consequatur temporibus. Vitae voluptates natus tempore quasi voluptatem quae rerum. Aut amet atque voluptates sunt ipsam exercitationem.', 'Geography Teacher', '2020-09-16 07:13:15', '2020-09-16 07:13:15', NULL),
(18, 'Mr.', 'Voluptate tempore magnam est non praesentium. Molestias dolor est rerum cumque voluptate esse aut. Magni dicta nam velit repellat excepturi necessitatibus odit.', 'Municipal Fire Fighting Supervisor', '2020-09-16 07:13:15', '2020-09-16 07:13:15', NULL),
(19, 'Mr.', 'Officia molestias tempora aut sint qui dignissimos aspernatur. Repudiandae voluptatem corporis in vitae assumenda et id. Deleniti esse maiores et molestiae accusamus quisquam.', 'Cabinetmaker', '2020-09-16 07:13:15', '2020-09-16 07:13:15', NULL),
(20, 'Dr.', 'Hic officiis et explicabo. Et ex magni sapiente consequatur voluptatum occaecati vel. Dolorem qui itaque et assumenda. Non in maiores sed consequatur et et.', 'Precision Printing Worker', '2020-09-16 07:13:16', '2020-09-16 07:13:16', NULL),
(21, 'Dr.', 'Dolor tenetur autem quia cupiditate occaecati quo et enim. Doloremque modi velit sit mollitia deleniti necessitatibus quae et. Molestias et et nostrum id doloremque.', 'Human Resources Manager', '2020-09-16 07:13:16', '2020-09-16 07:13:16', NULL),
(22, 'Mrs.', 'Earum id aut minus quam repellendus error accusantium. Corporis ipsa quas est explicabo blanditiis similique et atque. Alias voluptatem ullam neque qui tempora labore aut dignissimos. Minus molestiae qui quasi odit corrupti.', 'Massage Therapist', '2020-09-16 07:13:16', '2020-09-16 07:13:16', NULL),
(23, 'Dr.', 'Aut inventore ad voluptates debitis ipsa atque ipsam distinctio. Et nulla recusandae ut dolor magnam. Non commodi id ea sit incidunt.', 'Marking Machine Operator', '2020-09-16 07:13:16', '2020-09-16 07:13:16', NULL),
(24, 'Dr.', 'Nemo repudiandae sit incidunt sit quaerat excepturi aut doloremque. Sapiente rerum ut nulla consectetur optio. Totam nisi quis cum sit dolorem magni at dolorem. Necessitatibus nesciunt aliquam dolores.', 'Medical Scientists', '2020-09-16 07:13:16', '2020-09-16 07:13:16', NULL),
(25, 'Ms.', 'Sunt repudiandae enim ad labore. Necessitatibus ut est aut quam maxime ea. Et repellendus ratione est deserunt dolores. Dolore et laboriosam autem id.', 'Physician', '2020-09-16 07:13:16', '2020-09-16 07:13:16', NULL),
(26, 'Dr.', 'Fugiat iure fugit qui sequi sequi vel laboriosam. Sequi voluptatem quisquam assumenda sed qui eius delectus. Est vitae sequi magnam dolore. Tenetur et non tempora totam.', 'Storage Manager OR Distribution Manager', '2020-09-16 07:13:16', '2020-09-16 07:13:16', NULL),
(27, 'Dr.', 'Voluptas aliquid accusamus quia a eos. Cumque illum voluptate voluptatem quis minima.', 'Airframe Mechanic', '2020-09-16 07:13:16', '2020-09-16 07:13:16', NULL),
(28, 'Mrs.', 'Aut adipisci placeat id placeat vero. Repudiandae repellat perspiciatis cupiditate illum. Doloribus dolorem quos doloribus ad amet est.', 'Taper', '2020-09-16 07:13:16', '2020-09-16 07:13:16', NULL),
(29, 'Mr.', 'Sed rerum corrupti temporibus dolor fugiat pariatur fugiat illum. Delectus ratione nobis vero beatae fugiat molestiae necessitatibus. Nam ut vero non suscipit dolores. Laudantium animi est necessitatibus praesentium magni earum quidem. Animi eos quod quo enim.', 'Recreational Vehicle Service Technician', '2020-09-16 07:13:16', '2020-09-16 07:13:16', NULL),
(30, 'Mr.', 'Quos aspernatur consequuntur et ducimus aut. Eos rerum qui enim atque est itaque ab.', 'Artist', '2020-09-16 07:13:16', '2020-09-16 07:13:16', NULL),
(31, 'Mr.', 'Ea saepe rerum delectus nemo vel. Corporis nihil aut voluptas neque sint. Excepturi iusto autem dolore in repellendus quia. Eius explicabo eos ut.', 'Coroner', '2020-09-16 07:13:16', '2020-09-16 07:13:16', NULL),
(32, 'Prof.', 'Molestiae nisi praesentium in similique. Repudiandae doloribus dignissimos quibusdam vero qui. Rerum et aut possimus rem neque ducimus.', 'Massage Therapist', '2020-09-16 07:13:16', '2020-09-16 07:13:16', NULL),
(33, 'Ms.', 'Voluptatem autem laborum incidunt. Sed exercitationem commodi deserunt et sed ullam harum. Praesentium quam vel enim corporis.', 'Dragline Operator', '2020-09-16 07:13:16', '2020-09-16 07:13:16', NULL),
(34, 'Miss', 'Sequi pariatur iusto est et accusantium. Quae aut iure similique iure ut culpa. Ut similique ex et enim eligendi. Aspernatur voluptatum aut facere harum consectetur voluptatem.', 'Maintenance and Repair Worker', '2020-09-16 07:13:16', '2020-09-16 07:13:16', NULL),
(35, 'Mr.', 'Est officia cum sequi ut architecto architecto eligendi. Sequi nemo consequuntur necessitatibus velit eos velit eius fuga. Eum quibusdam natus sit deleniti nihil sit deserunt impedit. Consequuntur voluptatibus suscipit sit cupiditate dolorem cum.', 'Sawing Machine Setter', '2020-09-16 07:13:16', '2020-09-16 07:13:16', NULL),
(36, 'Mr.', 'Rerum aliquid eius ut numquam. Sunt et dolorem quia rerum ducimus est. Fuga excepturi dolores possimus magni ipsa odit. Labore fugiat culpa dolor eveniet ut nulla.', 'Farm Labor Contractor', '2020-09-16 07:13:16', '2020-09-16 07:13:16', NULL),
(37, 'Prof.', 'Nihil beatae facilis sit consectetur atque. Numquam nihil ut tempore consectetur. Culpa tempore et voluptas repellendus saepe animi placeat.', 'Agricultural Sciences Teacher', '2020-09-16 07:13:16', '2020-09-16 07:13:16', NULL),
(38, 'Dr.', 'Dolorum atque est eligendi dolorem itaque fugiat nemo. Facere quia inventore a. Vel cumque voluptatem excepturi itaque. Dolor impedit neque qui animi dolorem saepe eligendi.', 'Dot Etcher', '2020-09-16 07:13:16', '2020-09-16 07:13:16', NULL),
(39, 'Ms.', 'Quo quos voluptas qui quidem ad cupiditate minus. Ipsa et expedita accusamus nostrum perferendis quo autem. Deserunt maiores omnis ratione est laudantium quaerat. Veniam odit aperiam ratione sequi nulla provident dolorum.', 'Coaches and Scout', '2020-09-16 07:13:16', '2020-09-16 07:13:16', NULL),
(40, 'Mr.', 'Facere molestiae totam quos incidunt consequatur id voluptatem. Quasi voluptatem perspiciatis dolor. Autem tenetur aut dolorum accusamus illo quae.', 'Automotive Master Mechanic', '2020-09-16 07:13:17', '2020-09-16 07:13:17', NULL),
(41, 'Dr.', 'Sed qui hic est. Omnis eum odio explicabo aut quas. Fugiat et optio vel dolor sunt. Illo rerum laboriosam quidem fugit tempora ut dolorem.', 'Fire Fighter', '2020-09-16 07:13:17', '2020-09-16 07:13:17', NULL),
(42, 'Mr.', 'Minus a voluptas dolorem enim sapiente ipsa quia. Assumenda consequuntur culpa ipsam voluptatem sit dolor. Quos officiis velit quo enim autem. Aut asperiores earum molestiae.', 'Technical Program Manager', '2020-09-16 07:13:17', '2020-09-16 07:13:17', NULL),
(43, 'Prof.', 'Ea consectetur officiis et soluta repellendus odit aut. Fugit ut sed ad. Quo officia voluptas id magni ut et doloremque. Unde reprehenderit qui ipsum quo.', 'Keyboard Instrument Repairer and Tuner', '2020-09-16 07:13:17', '2020-09-16 07:13:17', NULL),
(44, 'Prof.', 'Nulla libero aperiam ducimus qui dolores voluptatem. Qui voluptas similique dolores dolorem quibusdam qui.', 'Technical Specialist', '2020-09-16 07:13:17', '2020-09-16 07:13:17', NULL),
(45, 'Mrs.', 'Illo quisquam et illum exercitationem aliquid consequatur. Culpa sed ea ut. Ut necessitatibus nobis reiciendis vel mollitia sit. Animi pariatur at earum molestiae possimus delectus odit.', 'Web Developer', '2020-09-16 07:13:17', '2020-09-16 07:13:17', NULL),
(46, 'Dr.', 'Dolorem qui et sit. In expedita velit rerum ratione aperiam deleniti. Quaerat odit aut velit harum mollitia. Fugit autem excepturi voluptas est.', 'Tractor Operator', '2020-09-16 07:13:17', '2020-09-16 07:13:17', NULL),
(47, 'Ms.', 'Suscipit optio voluptate sed aut facilis nulla sint. Expedita maxime eum inventore placeat voluptas. Ut qui molestiae et.', 'Web Developer', '2020-09-16 07:13:17', '2020-09-16 07:13:17', NULL),
(48, 'Prof.', 'Labore ea totam et voluptas ut nisi quod. Perferendis cumque voluptates voluptatem omnis temporibus aspernatur. Omnis labore quas consequatur porro.', 'Elementary and Secondary School Administrators', '2020-09-16 07:13:17', '2020-09-16 07:13:17', NULL),
(49, 'Mrs.', 'Minima magni et et dolor corporis accusantium eveniet. Ducimus illo aliquid eligendi et. In officiis et aut excepturi voluptatem nobis nihil. Sunt cumque eum odio ut.', 'Psychiatric Technician', '2020-09-16 07:13:17', '2020-09-16 07:13:17', NULL),
(50, 'Dr.', 'Iure labore velit ad repellendus perspiciatis necessitatibus sed perspiciatis. Cum molestiae modi impedit officia dignissimos.', 'Lay-Out Worker', '2020-09-16 07:13:17', '2020-09-16 07:13:17', NULL),
(51, 'Ms.', 'Possimus soluta dolorem quos molestiae vero libero praesentium. Qui voluptatem quod occaecati sint hic perferendis. Rerum et qui consequatur quidem et.', 'Corporate Trainer', '2020-09-16 07:13:17', '2020-09-16 07:13:17', NULL),
(52, 'Dr.', 'Est molestiae occaecati aliquid sint. Suscipit labore perspiciatis est quam est. Temporibus vel eaque in qui. Autem quas dolor a praesentium qui itaque omnis praesentium.', 'Cafeteria Cook', '2020-09-16 07:13:17', '2020-09-16 07:13:17', NULL),
(53, 'Prof.', 'Quis quidem saepe nihil a. Et in quia delectus quam sit. Aut quasi facere et reiciendis quas natus quidem.', 'Executive Secretary', '2020-09-16 07:13:17', '2020-09-16 07:13:17', NULL),
(54, 'Prof.', 'Architecto ipsam molestias animi autem veniam et nulla. Repellat sint animi corporis aliquam debitis sit excepturi. Ipsam quia mollitia omnis nihil eum pariatur. Cumque nihil id asperiores perspiciatis.', 'Administrative Services Manager', '2020-09-16 07:13:17', '2020-09-16 07:13:17', NULL),
(55, 'Prof.', 'Non consequatur ipsa rerum alias. Rerum pariatur hic iure sequi consequatur similique. Architecto debitis voluptas veritatis ea quo. Beatae temporibus nulla dolorum est earum quam quo. Amet nihil et soluta fugit et.', 'Freight Inspector', '2020-09-16 07:13:18', '2020-09-16 07:13:18', NULL),
(56, 'Dr.', 'Blanditiis ut nesciunt earum alias enim. Tempora magni et quam laboriosam. Enim rerum rem quae blanditiis. Voluptatum esse quia sed optio dicta rem.', 'Prepress Technician', '2020-09-16 07:13:18', '2020-09-16 07:13:18', NULL),
(57, 'Dr.', 'Ut odit excepturi quam nostrum voluptate in omnis. Et mollitia et corporis aut. Veritatis doloribus ea id. Sit eum sit ex perferendis aut repudiandae.', 'Mechanical Drafter', '2020-09-16 07:13:18', '2020-09-16 07:13:18', NULL),
(58, 'Prof.', 'Magnam maiores tempore aliquam molestias error illum autem et. Omnis qui iusto nihil vitae deserunt.', 'Forester', '2020-09-16 07:13:18', '2020-09-16 07:13:18', NULL),
(59, 'Prof.', 'Aliquid qui laudantium amet maiores commodi. Fuga officia qui veritatis perferendis earum et. Quaerat sapiente est dignissimos corrupti impedit magni velit. Sapiente cupiditate rerum aut corrupti.', 'School Bus Driver', '2020-09-16 07:13:18', '2020-09-16 07:13:18', NULL),
(60, 'Ms.', 'Ea eaque impedit praesentium dolorem quis ipsa et. Quia autem odio hic harum aspernatur tempore vel adipisci. Distinctio provident odit cum natus voluptatum. Velit natus animi esse voluptatem qui et laudantium. Commodi a neque ratione ut nihil saepe.', 'Structural Metal Fabricator', '2020-09-16 07:13:18', '2020-09-16 07:13:18', NULL),
(61, 'Mr.', 'Sequi officiis dolorem totam ea. Ullam eos esse harum. Facilis placeat voluptatibus voluptatem sequi sit maxime vel. Fugiat magnam quia cupiditate.', 'Anthropologist OR Archeologist', '2020-09-16 07:13:18', '2020-09-16 07:13:18', NULL),
(62, 'Prof.', 'Molestias odit aliquid tenetur ea sequi distinctio. Et est quaerat et quisquam perferendis nulla. Iure suscipit pariatur animi at placeat impedit officiis.', 'Precision Pattern and Die Caster', '2020-09-16 07:13:18', '2020-09-16 07:13:18', NULL),
(63, 'Prof.', 'Nulla non explicabo consequatur autem qui consequatur aliquid. Qui repudiandae ab expedita illo et. Autem recusandae consequuntur quos reiciendis magni aperiam et. Cumque id ut facere explicabo id similique nihil.', 'Chemical Plant Operator', '2020-09-16 07:13:18', '2020-09-16 07:13:18', NULL),
(64, 'Ms.', 'Sequi quibusdam esse facilis sunt iusto aut. Consectetur amet consequatur ut quam. Quia eius in rem ab.', 'Fabric Mender', '2020-09-16 07:13:18', '2020-09-16 07:13:18', NULL),
(65, 'Mrs.', 'Id optio aut nobis ut modi. Ipsa aperiam odit minus corrupti dolores quia eveniet. Tempora perferendis architecto sapiente quam sunt hic. Explicabo est minus quae sunt inventore adipisci eos.', 'Receptionist and Information Clerk', '2020-09-16 07:13:18', '2020-09-16 07:13:18', NULL),
(66, 'Mr.', 'Sed sed exercitationem ducimus voluptatem. Reprehenderit quam ut molestiae quidem voluptatem. Accusantium voluptatem eum aliquam quibusdam aut. Laudantium unde odio eius dolores. Et corporis non qui.', 'Floor Finisher', '2020-09-16 07:13:18', '2020-09-16 07:13:18', NULL),
(67, 'Mr.', 'Ad ut quia amet laborum eum. Ut eum rerum aperiam sunt. Porro accusantium id qui aliquam laudantium.', 'Dietetic Technician', '2020-09-16 07:13:18', '2020-09-16 07:13:18', NULL),
(68, 'Mr.', 'Tempora sit voluptatem tempore. Sunt voluptatum ullam reiciendis voluptatem veniam. Qui ad qui omnis veniam optio eius in. Accusantium atque ratione qui itaque libero sunt sint.', 'Tool Set-Up Operator', '2020-09-16 07:13:18', '2020-09-16 07:13:18', NULL),
(69, 'Prof.', 'Voluptatibus sed aut omnis architecto et explicabo. Nihil consequatur et quibusdam. Aut quis expedita perspiciatis. Rerum recusandae autem animi voluptas.', 'Copy Writer', '2020-09-16 07:13:18', '2020-09-16 07:13:18', NULL),
(70, 'Prof.', 'Debitis ut sint omnis et. Incidunt iste molestias dolores minima a. Atque eum et voluptatem id ex ducimus eaque alias. Expedita omnis corporis quasi consequatur ullam.', 'Metal-Refining Furnace Operator', '2020-09-16 07:13:18', '2020-09-16 07:13:18', NULL),
(71, 'Mr.', 'Et expedita et quos sapiente. Pariatur nobis et corporis voluptas dolores aliquid. Quod quis similique voluptate rerum. Iusto aperiam eveniet excepturi eaque.', 'Chemical Technician', '2020-09-16 07:13:18', '2020-09-16 07:13:18', NULL),
(72, 'Ms.', 'Aut vel expedita aut. Repellat fuga iste et. Molestiae blanditiis qui molestiae fugiat eveniet rem totam.', 'Social Media Marketing Manager', '2020-09-16 07:13:19', '2020-09-16 07:13:19', NULL),
(73, 'Dr.', 'Et repellendus nemo veritatis sint. Quis facere laboriosam ipsam. Et et voluptatum laboriosam. Cupiditate aut qui est occaecati.', 'Actor', '2020-09-16 07:13:19', '2020-09-16 07:13:19', NULL),
(74, 'Prof.', 'Pariatur et perspiciatis non consequuntur odio. Aut aut est optio consequuntur cumque a reiciendis. Deleniti laudantium sapiente nemo autem. Facilis consequatur recusandae fugiat. Est magnam quo voluptatem laborum rem architecto cum.', 'Roofer', '2020-09-16 07:13:19', '2020-09-16 07:13:19', NULL),
(75, 'Prof.', 'Autem iusto ullam vel aut ea sit tempore magnam. Exercitationem amet laboriosam suscipit omnis et ipsa. Placeat maiores repudiandae reiciendis.', 'Food Science Technician', '2020-09-16 07:13:19', '2020-09-16 07:13:19', NULL),
(76, 'Dr.', 'A non repudiandae dignissimos ducimus dolores hic minus. Sapiente rerum culpa sint. Dolore minima voluptas qui est.', 'Substation Maintenance', '2020-09-16 07:13:19', '2020-09-16 07:13:19', NULL),
(77, 'Ms.', 'Nostrum beatae vitae iusto facere est voluptas. Sunt laborum quam soluta perferendis enim dignissimos sit. Non vero error in rerum et pariatur consequatur quia. Architecto nihil rerum similique architecto.', 'Transportation Worker', '2020-09-16 07:13:19', '2020-09-16 07:13:19', NULL),
(78, 'Mr.', 'Asperiores nesciunt et et aut perferendis. Labore ducimus libero blanditiis. Nobis voluptatibus cum debitis nulla. Et enim dolorem assumenda ut.', 'Rail Yard Engineer', '2020-09-16 07:13:19', '2020-09-16 07:13:19', NULL),
(79, 'Dr.', 'Aspernatur et labore illo tempora id et incidunt. Nihil et et totam omnis et repellendus totam. Reiciendis maiores qui provident libero nulla ex. At delectus eius magni.', 'Social Service Specialists', '2020-09-16 07:13:19', '2020-09-16 07:13:19', NULL),
(80, 'Mr.', 'Voluptas ea ea alias praesentium veniam repellat dolores. Sed fugit et vero porro voluptatum quos. Aut reiciendis dolorem asperiores non sit a omnis.', 'Plant Scientist', '2020-09-16 07:13:19', '2020-09-16 07:13:19', NULL),
(81, 'Prof.', 'Enim est voluptas corrupti totam ut. Et mollitia sed nobis quis eius non sint. Eligendi adipisci sint qui enim nemo autem dolores. Dolores nemo aliquid similique consequatur.', 'Parking Enforcement Worker', '2020-09-16 07:13:19', '2020-09-16 07:13:19', NULL),
(82, 'Mr.', 'Dolor perspiciatis aut pariatur quia aut consectetur. Voluptas libero possimus doloremque placeat enim. Distinctio voluptatum aperiam suscipit et aut.', 'Precision Aircraft Systems Assemblers', '2020-09-16 07:13:19', '2020-09-16 07:13:19', NULL),
(83, 'Dr.', 'Sed corporis iure sint porro quos dolor. Aliquam modi praesentium vel saepe rerum ut. Enim similique repellendus aut in est voluptatem.', 'Data Processing Equipment Repairer', '2020-09-16 07:13:19', '2020-09-16 07:13:19', NULL),
(84, 'Dr.', 'Unde aut voluptas aperiam vitae. Possimus exercitationem pariatur consequatur provident eveniet minus vel. Deserunt esse provident maxime rem repudiandae.', 'Life Science Technician', '2020-09-16 07:13:19', '2020-09-16 07:13:19', NULL),
(85, 'Prof.', 'Omnis a at vero. Non non eaque distinctio.', 'Garment', '2020-09-16 07:13:19', '2020-09-16 07:13:19', NULL),
(86, 'Dr.', 'Ex aut dolor ut delectus ut provident. Tempora et cupiditate deserunt nesciunt pariatur. Eligendi numquam nobis nam sed consectetur qui consequuntur.', 'Able Seamen', '2020-09-16 07:13:19', '2020-09-16 07:13:19', NULL),
(87, 'Ms.', 'Nihil laboriosam quo reprehenderit est et aliquam est. Voluptas illum repudiandae quaerat ut. Qui tempore aliquid officiis eligendi quae. Quibusdam sequi officia vel voluptatem corrupti molestiae aut et.', 'Aircraft Rigging Assembler', '2020-09-16 07:13:19', '2020-09-16 07:13:19', NULL),
(88, 'Mr.', 'Nihil officia voluptatem harum inventore autem. Eum nihil et velit omnis. Dolore iure delectus illum doloribus earum numquam. Aliquam quidem consequatur et ipsum.', 'Ambulance Driver', '2020-09-16 07:13:19', '2020-09-16 07:13:19', NULL),
(89, 'Mr.', 'Reiciendis in excepturi voluptates aliquid animi excepturi ut. Fugit iste et quia laborum eaque facere occaecati. Ratione ullam tenetur sunt voluptas eveniet ab ut dolorum.', 'Dental Laboratory Technician', '2020-09-16 07:13:19', '2020-09-16 07:13:19', NULL),
(90, 'Prof.', 'Est incidunt repellat inventore tempora possimus. Ducimus tempora eos repellat expedita sint. Vel temporibus fuga perferendis doloribus et debitis possimus. Rerum libero sint ut quo quia praesentium.', 'Freight and Material Mover', '2020-09-16 07:13:19', '2020-09-16 07:13:19', NULL),
(91, 'Mr.', 'Explicabo laudantium rem et aliquid vel consequuntur repellendus accusamus. Aperiam rerum incidunt repellendus dolor reprehenderit sed. Dolore voluptatibus quo fugiat eos vel.', 'Power Generating Plant Operator', '2020-09-16 07:13:19', '2020-09-16 07:13:19', NULL),
(92, 'Dr.', 'Dolorum rem porro ipsa. Earum eum pariatur consequatur et sed. Dolor doloremque unde provident.', 'Cement Mason and Concrete Finisher', '2020-09-16 07:13:19', '2020-09-16 07:13:19', NULL),
(93, 'Prof.', 'Expedita recusandae unde qui temporibus. Enim accusantium autem excepturi itaque. Aut voluptatem eveniet explicabo. Sint voluptatem est eveniet enim similique.', 'Nuclear Medicine Technologist', '2020-09-16 07:13:20', '2020-09-16 07:13:20', NULL),
(94, 'Mrs.', 'Eaque placeat voluptas deleniti et est. Asperiores iure impedit voluptatem modi cupiditate. Repudiandae nihil tempora ut dolores sint.', 'Heaters', '2020-09-16 07:13:20', '2020-09-16 07:13:20', NULL),
(95, 'Prof.', 'Sapiente aspernatur iusto asperiores non in maiores. Mollitia quis veniam molestiae fugit minus et voluptate aut. Et explicabo non et pariatur qui voluptas reprehenderit ullam. Nihil mollitia explicabo quis.', 'Distribution Manager', '2020-09-16 07:13:20', '2020-09-16 07:13:20', NULL),
(96, 'Dr.', 'Voluptas dignissimos eos error itaque est aperiam. Suscipit non tempora ratione ea ea. Enim voluptatibus non vel asperiores quasi architecto. Magnam odio id sit est.', 'Administrative Services Manager', '2020-09-16 07:13:20', '2020-09-16 07:13:20', NULL),
(97, 'Dr.', 'Ea dolor quia dolorem officia quaerat quis accusamus. Voluptatem reiciendis cumque ex facilis cum. Repudiandae commodi sunt id et dolor. Iusto debitis eum magnam accusamus iste porro mollitia.', 'Dental Assistant', '2020-09-16 07:13:20', '2020-09-16 07:13:20', NULL),
(98, 'Miss', 'Non sequi rerum voluptas earum numquam veritatis ad. Voluptas et ut tenetur inventore assumenda illum. Molestiae nemo pariatur ad quia voluptas. Doloremque quod qui dolores est neque voluptatem maiores dolor.', 'Roof Bolters Mining', '2020-09-16 07:13:20', '2020-09-16 07:13:20', NULL),
(99, 'Mr.', 'Distinctio et id omnis. Eveniet ad saepe earum ut commodi. Fugit et ipsum dolorum non occaecati. Inventore vitae unde non.', 'Poultry Cutter', '2020-09-16 07:13:20', '2020-09-16 07:13:20', NULL),
(100, 'Dr.', 'Repellendus cum magnam sequi dolores. Consequatur ea et et tempora dolorem tempore. Tenetur totam in vel enim et quis. Aliquid earum aut facilis id.', 'Platemaker', '2020-09-16 07:13:20', '2020-09-16 07:13:20', NULL),
(101, 'Mrs.', 'Eveniet et et numquam quae aut voluptatem. Voluptatum quasi voluptates culpa. Quia magni officiis alias similique aperiam quae. Ullam qui quod totam qui debitis quae accusamus.', 'Biological Science Teacher', '2020-09-16 07:13:20', '2020-09-16 07:13:20', NULL),
(102, 'Ms.', 'Quam quasi nostrum consequatur perferendis fugiat deleniti in blanditiis. Eum delectus ut maxime rerum sapiente ipsam aliquam aliquid. Rerum dolor laboriosam facere et non. Repellat tenetur non iure ex quod est.', 'Writer OR Author', '2020-09-16 07:13:20', '2020-09-16 07:13:20', NULL),
(103, 'Dr.', 'Necessitatibus harum quibusdam sed commodi quibusdam. Quia quod quas et ullam non maxime. Perspiciatis est ab deserunt maxime voluptatem. Et occaecati sit voluptatem occaecati quos dolorem.', 'Human Resource Director', '2020-09-16 07:13:20', '2020-09-16 07:13:20', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_09_15_123854_create_blog_table', 1),
(5, '2020_09_15_125306_add_category_in_blog_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `blog`
--
ALTER TABLE `blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `blog`
--
ALTER TABLE `blog`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=104;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
