<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogTable extends Migration {
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('blog', function (Blueprint $table) {
            //Auto increment, integer, Primary key
            $table->increments('id');
            $table->string('title', 150);
            $table->text('content');
            //membuat field created_at, updated_at
            $table->timestamps();
            //membuat field deleted_at
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('blog');
    }
}
